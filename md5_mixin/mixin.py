import hashlib
from django.db import models
from django.contrib.postgres.fields import HStoreField


class Md5HashMixin(models.Model):
    md5_check_on_save = True
    md5_callbacks = None
    md5_hash = HStoreField(null=True, blank=True, default=None)

    class Meta:
        abstract = True

    def _md5_calculate_hash_for_fields(self, fields):
        """Считаем хеш для конкретного набора полей"""
        hash_values = []
        for field in fields:
            field_info = self._meta.get_field(field)
            if field_info.many_to_one:  # Foreign
                hash_values.append(getattr(self, f"{field}_id", None))
            elif field_info.many_to_many:  # ManyToManyField
                _ids = list(getattr(self, field).all().values_list('id', flat=True).order_by('id'))
                hash_values.append(",".join(map(str, _ids)))
            elif isinstance(getattr(self, field, ""), list):
                hash_values.append(",".join(map(str, getattr(self, field, ""))))
            else:
                hash_values.append(getattr(self, field, ""))
        hash_str = ':'.join(map(str, hash_values))
        # print(f"{fields} -> {hash_str}")
        return hashlib.md5(hash_str.encode('utf-8')).hexdigest()

    def _md5_calculate_hash(self):
        """Считаем полный хеш"""
        new_hash = {}
        for group in self.md5_callbacks:
            for item in group:
                name = item.get('name', 'default')
                new_hash[name] = self._md5_calculate_hash_for_fields(item['fields'])
        return new_hash

    def _md5_hashes_equal(self, h1, h2):
        all_keys = set(h1.keys()) | set(h2.keys())
        for key in all_keys:
            if h1.get(key, None) != h2.get(key, None):
                return False
        return True

    def _md5_proceed_item(self, item):
        name = item.get('name', 'default')
        old_hash = self.md5_hash.get(name)
        new_hash = self._md5_calculate_hash(item['fields'])
        if old_hash != new_hash:
            self.md5_hash[name] = new_hash
            # Запускаем колбэк
            callback = item.get('callback', f"md5_{name}")
            getattr(self, callback)()
            return True
        return False

    def md5_check(self):
        if self.md5_hash is None:
            self.md5_hash = {}
        new_hash = self._md5_calculate_hash()
        if self._md5_hashes_equal(self.md5_hash, new_hash):
            return  # ничего не поменялось, все ок
        else:
            for group in self.md5_callbacks:
                for item in group:
                    name = item.get('name', 'default')
                    if self.md5_hash.get(name, None) != new_hash[name]:
                        # изменилось значение -- запускаем колбэк
                        callback = item.get('callback', f"md5_{name}")
                        getattr(self, callback)()
                        break  # если колбэк сработал, то сразу переходим к следующей группе

            self.md5_hash = new_hash
            self.__class__.objects.filter(id=self.id).update(md5_hash=new_hash)

    def save(self, *args, **kwargs):
        super().save(*args, **kwargs)
        if self.md5_check_on_save:
            self.md5_check()
