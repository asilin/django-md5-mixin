from django.test import TestCase
from unittest.mock import MagicMock, patch
from test_module.models import Book, Author, Genre


@patch.object(Author, 'md5_default')
@patch.object(Book, 'md5_patch')
@patch.object(Book, 'md5_update')
@patch.object(Book, 'md5_published')
class BasicTestCase(TestCase):
    def setUp(self) -> None:
        g1 = Genre(name='Приключения')
        g1.save()
        g2 = Genre(name='Фантастика')
        g2.save()
        g3 = Genre(name='Любовный роман')
        g3.save()

        adams = Author(name='Дуглас Адамс', year=1952)
        adams.save()

        wells = Author(name='Герберт Уэллс', year=1866)
        wells.save()

        b = Book(name='Автостопом по галактике', year=1979, author=adams)
        b.save()
        b.genres.set([g1, g2])
        b.md5_check()  # т.к. автоматом оно не посчитается

    def tearDown(self) -> None:
        Genre.objects.all().delete()
        Book.objects.all().delete()
        Author.objects.all().delete()

    def test_author(self, *args):
        author = Author.objects.get(id=1)
        assert author.id == 1

        author.name = 'Поменяли'
        author.save()
        self.assertEqual(author.md5_default.call_count, 1)

        author.save()
        self.assertEqual(author.md5_default.call_count, 1)

        author.year = 2022
        author.save()
        self.assertEqual(author.md5_default.call_count, 2)

    def test_book_published(self, *args):
        book = Book.objects.get(name='Автостопом по галактике')

        book.published = False
        book.save()
        # после сохраниенмя ничего не произошло (автоматическая обработка выключена)
        self.assertEqual(book.md5_published.call_count, 0)
        self.assertEqual(book.md5_update.call_count, 0)
        self.assertEqual(book.md5_patch.call_count, 0)

        book.md5_check()
        self.assertEqual(book.md5_published.call_count, 1)
        self.assertEqual(book.md5_update.call_count, 1)
        self.assertEqual(book.md5_patch.call_count, 0)

        book.save()
        book.md5_check()
        # после повторной провреки ничего не призошло, т.к. изменений нет
        self.assertEqual(book.md5_published.call_count, 1)
        self.assertEqual(book.md5_update.call_count, 1)
        self.assertEqual(book.md5_patch.call_count, 0)

        book.published = True
        book.save()
        # ничего не поменялось ,тк. не запущен чек
        self.assertEqual(book.md5_published.call_count, 1)
        self.assertEqual(book.md5_update.call_count, 1)
        self.assertEqual(book.md5_patch.call_count, 0)

        book.md5_check()
        self.assertEqual(book.md5_published.call_count, 2)
        self.assertEqual(book.md5_update.call_count, 2)
        self.assertEqual(book.md5_patch.call_count, 0)

    def test_book_update(self, *args):
        book = Book.objects.get(name='Автостопом по галактике')
        adams = Author.objects.get(name='Дуглас Адамс')
        wells = Author.objects.get(name='Герберт Уэллс')
        self.assertEqual(book.author_id, adams.id)

        self.assertEqual(book.md5_published.call_count, 0)
        self.assertEqual(book.md5_update.call_count, 0)
        self.assertEqual(book.md5_patch.call_count, 0)

        book.author = wells
        book.save()

        self.assertEqual(book.md5_published.call_count, 0)
        self.assertEqual(book.md5_update.call_count, 0)
        self.assertEqual(book.md5_patch.call_count, 0)

        book.md5_check()

        self.assertEqual(book.md5_published.call_count, 0)
        self.assertEqual(book.md5_update.call_count, 1)
        self.assertEqual(book.md5_patch.call_count, 0)

    def test_book_patch(self, *args):
        book = Book.objects.get(name='Автостопом по галактике')
        book.name += 'UPDATED'
        book.save()
        book.md5_check()
        self.assertEqual(book.md5_published.call_count, 0)
        self.assertEqual(book.md5_update.call_count, 0)
        self.assertEqual(book.md5_patch.call_count, 1)

    def test_book_many2many(self, *args):
        book = Book.objects.get(name='Автостопом по галактике')
        g3 = Genre.objects.get(name='Любовный роман')
        book.genres.add(g3)

        self.assertEqual(book.md5_published.call_count, 0)
        self.assertEqual(book.md5_update.call_count, 0)
        self.assertEqual(book.md5_patch.call_count, 0)

        book.md5_check()

        self.assertEqual(book.md5_published.call_count, 0)
        self.assertEqual(book.md5_update.call_count, 1)
        self.assertEqual(book.md5_patch.call_count, 0)




