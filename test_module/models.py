from django.db import models

from md5_mixin.mixin import Md5HashMixin


class Author(Md5HashMixin, models.Model):
    name = models.CharField(max_length=128)
    year = models.IntegerField(null=True, default=None, blank=True)

    md5_callbacks = [
        [{'fields': ['name', 'year']}]
    ]

    def md5_default(self):
        ...


class Genre(models.Model):
    name = models.CharField(max_length=128)


class Book(Md5HashMixin, models.Model):
    name = models.CharField(max_length=128)
    year = models.IntegerField(null=True, default=None, blank=True)
    published = models.BooleanField(default=True)
    author = models.ForeignKey(Author, on_delete=models.CASCADE, null=True, default=None, blank=True)
    genres = models.ManyToManyField(Genre)

    md5_check_on_save = False  # так как есть many2many
    md5_callbacks = [
        [
            {'name': 'published', 'fields': ['published']},
        ],
        [
            {'name': 'update', 'fields': ['author', 'genres', 'published']},
            {'name': 'patch', 'fields': ['name', 'year']},
        ]
    ]

    class Meta:
        verbose_name = 'Книга'

    def md5_patch(self):
        pass

    def md5_update(self):
        pass

    def md5_published(self):
        pass
